from os import path, listdir, mkdir
from shutil import rmtree
from natsort import natsorted
import re
import pickle
import zstd

# from PIL.Image import Image
# from PIL.ImageQt import ImageQt
# import threading
from hashlib import sha1, md5
from PySide2 import QtWidgets, QtGui, QtCore

from GUI.window import Ui_MainWindow
from GUI.ImageForm import Ui_ImageForm


class GUIImageForm(Ui_ImageForm, QtWidgets.QTabWidget):
    def __init__(self, pickle_file_path: str, cache_dir: str, image_folder_name: str):
        super(GUIImageForm, self).__init__()
        self.setupUi(self)

        scale_images: bool = False

        # Derive individual cache folder path
        suffix_len: int = len(".pickle")
        cache_file_name_base = path.join(
            cache_dir,
            image_folder_name + "_" + path.basename(pickle_file_path)[:-suffix_len],
        )

        # Load all images from cache, since Qt is strange when loading PIL files
        original_img_path: str = path.join(cache_file_name_base, "original_img.png")
        original_img_pixmap = QtGui.QPixmap(original_img_path)
        self.graphics_original.setPixmap(original_img_pixmap)
        self.graphics_original.setScaledContents(scale_images)

        mask_path: str = path.join(cache_file_name_base, "msk_colour.png")
        mask_pixmap = QtGui.QPixmap(mask_path)
        self.graphics_mask.setPixmap(mask_pixmap)
        self.graphics_mask.setScaledContents(scale_images)

        output_img_path_list: list = [
            path.normpath(path.join(cache_file_name_base, x))
            for x in listdir(cache_file_name_base)
            if ".png" in x and "output_img" in x
        ]

        feature_extraction_path_list: list = [
            path.normpath(path.join(cache_file_name_base, x))
            for x in listdir(cache_file_name_base)
            if ".png" in x and "feature_selection" in x
        ]

        self.graphics_output_img_list: list = []
        self.graphics_feature_img_list: list = []

        for i, output_img_path in enumerate(output_img_path_list):
            output_img_pixmap = QtGui.QPixmap(output_img_path)

            graphics_output_img = QtWidgets.QLabel()
            graphics_output_img.setPixmap(output_img_pixmap)
            graphics_output_img.setScaledContents(scale_images)
            graphics_output_img.setAlignment(QtCore.Qt.AlignCenter)
            self.tab_output_img_widget.addTab(
                graphics_output_img, "Output Image {i}".format(i=i)
            )
            self.graphics_output_img_list.append(graphics_output_img)

        for i, feature_img_path in enumerate(feature_extraction_path_list):
            feature_img_pixmap = QtGui.QPixmap(feature_img_path)

            graphics_feature_img = QtWidgets.QLabel()
            graphics_feature_img.setPixmap(feature_img_pixmap)
            graphics_feature_img.setScaledContents(scale_images)
            graphics_feature_img.setAlignment(QtCore.Qt.AlignCenter)
            self.tab_feature_img_widget.addTab(
                graphics_feature_img, "Feature Image {i}".format(i=i)
            )
            self.graphics_feature_img_list.append(graphics_feature_img)

    # Get any state info that should be carried over when recreated
    def get_current_state(self):
        return self.tab_output_img_widget.currentIndex()

    # Set state info when recreated
    def set_current_state(self, state):
        tab_index: int = state["tab_index"]
        self.tab_output_img_widget.setCurrentIndex(tab_index)


class GUIMainWindow(Ui_MainWindow, QtWidgets.QMainWindow):
    def __init__(self):
        super(GUIMainWindow, self).__init__()
        self.setupUi(self)
        self.main_title: str = "Masters Project Image Viewer"
        self.setWindowTitle(self.main_title)

        self.thread_pool = QtCore.QThreadPool()

        # Signals
        self.button_load.clicked.connect(self.load_image_list)
        self.button_select_directory.clicked.connect(self.select_folder)
        self.image_title_list_widget.itemSelectionChanged.connect(
            self.image_title_list_widget_item_selected
        )
        self.button_invalidate_cache.clicked.connect(self.clear_cache)

        # Image cache
        self.cache_dir: str = path.normpath("./.cache")
        if not path.exists(self.cache_dir):
            mkdir(self.cache_dir)

        # Storage
        self.image_folder_name: str = ""
        self.file_names_by_epoch: dict = {}
        self.image_form_list: list = []
        self.num_tabs: int = -1
        self.current_tab: int = -1

    def keyPressEvent(self, event: QtGui.QKeyEvent):
        super(GUIMainWindow, self).keyPressEvent(event)
        if event.key() == 16777234:
            self.tab_image_widget.setCurrentIndex(
                self.tab_image_widget.currentIndex() - 1
            )
        if event.key() == 16777236:
            self.tab_image_widget.setCurrentIndex(
                self.tab_image_widget.currentIndex() + 1
            )

    def clear_cache(self):
        rmtree(self.cache_dir, ignore_errors=True)
        mkdir(self.cache_dir)

    def image_title_list_widget_item_selected(self, item=None):
        if not item:
            item = self.image_title_list_widget.currentItem()
        pickle_file_names: list = natsorted(self.file_names_by_epoch[item.epoch_key])
        self.num_tabs = len(pickle_file_names)
        # print(file_names)

        # Get state from tabs
        self.current_tab: int = self.tab_image_widget.currentIndex()
        current_image_form_tabs_state: list = [
            x.get_current_state() for x in self.image_form_list if x
        ]

        # Delete current tabs
        for i, form in enumerate(self.image_form_list):
            if form:
                form.setParent(None)
                form.deleteLater()
        self.tab_image_widget.clear()
        self.image_form_list.clear()
        # self.image_form_list = []

        # Create tabs and image forms
        for index, pickle_file_name in enumerate(pickle_file_names):

            # Derive individual cache folder path
            suffix_len: int = len(".pickle")
            cache_file_name_base = path.join(
                self.cache_dir,
                self.image_folder_name
                + "_"
                + path.basename(pickle_file_name)[:-suffix_len],
            )
            # print(cache_file_name_base)

            # Hashing, may use later
            # with open(pickle_file_path, "rb") as pickle_file:
            #     BLOCK_SIZE = 65536
            #     file_hash = sha1()
            #     file_block = pickle_file.read(BLOCK_SIZE)
            #     while len(file_block) > 0:
            #         file_hash.update(file_block)
            #         file_block = pickle_file.read(BLOCK_SIZE)
            #
            #     hash_code = file_hash.hexdigest()
            #     print(hash_code)

            # If the cache folder does not exist, create cache
            if not path.exists(cache_file_name_base):
                cache_maker: CacheMaker = CacheMaker(
                    self, pickle_file_name, cache_file_name_base
                )
                # image_form_loader.emitter.done.connect(self.create_tabs_after_processing)
                self.thread_pool.start(cache_maker)

        self.thread_pool.waitForDone()

        for index, pickle_file_name in enumerate(pickle_file_names):
            image_form = GUIImageForm(
                pickle_file_name, self.cache_dir, self.image_folder_name
            )
            # Todo: fix when coming from smaller to larger output images
            state: dict = {
                "tab_index": current_image_form_tabs_state[index]
                if len(current_image_form_tabs_state) > 0
                and len(self.image_form_list) > 0
                else -1
            }

            if state["tab_index"] > -1:
                image_form.set_current_state(state=state)
            self.image_form_list.append(image_form)
            # self.emitter.done.emit(str(self.name))
            image_form.moveToThread(self.thread())
            # image_form.setParent(self.Main)
            self.tab_image_widget.addTab(
                image_form, "Figure {num}".format(num=index),
            )
        if self.current_tab != -1:
            self.tab_image_widget.setCurrentIndex(self.current_tab)

    def select_folder(self):
        folder_path = QtWidgets.QFileDialog.getExistingDirectory(
            self, "Select Image Folder"
        )
        if folder_path:
            self.text_edit_directory.setPlainText(folder_path)
            self.load_image_list()

    def load_image_list(self):
        # Delete objects
        for i in reversed(range(self.image_title_list_widget.count())):
            self.image_title_list_widget.removeItemWidget(
                self.image_title_list_widget.item(i)
            )
        for i in self.image_form_list:
            i.deleteLater()

        # Clear containers
        self.image_title_list_widget.clear()
        self.tab_image_widget.clear()
        self.file_names_by_epoch.clear()
        self.image_form_list.clear()

        # Name of folder to load images from
        folder_name: str = path.normpath(self.text_edit_directory.toPlainText())
        if not path.exists(folder_name):
            return

        self.image_folder_name = path.basename(folder_name)
        self.setWindowTitle(
            "{main_title}: {folder_name}".format(
                main_title=self.main_title, folder_name=self.image_folder_name
            )
        )

        # Get list names of compatible files
        loaded_file_names = natsorted(
            [path.join(folder_name, x) for x in listdir(folder_name) if ".pickle" in x]
        )

        file_name: str
        for file_name in loaded_file_names:
            # Split name into list
            split_name: list = re.split(r"[;,_.\s]", file_name)

            # Find epoch of file
            index_epoch = split_name.index("epoch") + 1
            if split_name[index_epoch] not in self.file_names_by_epoch.keys():
                self.file_names_by_epoch.update({(str(split_name[index_epoch])): []})

            self.file_names_by_epoch[str(split_name[index_epoch])].append(
                path.abspath(file_name)
            )

        epoch_list = natsorted(self.file_names_by_epoch.keys())

        for epoch_key in epoch_list:
            item = QtWidgets.QListWidgetItem(self.image_title_list_widget)
            item.setText("Epoch {num}".format(num=epoch_key))
            item.epoch_key = epoch_key
        pass


class CacheMaker(QtCore.QRunnable):
    def __init__(
        self, parent: GUIMainWindow, pickle_file_path: str, cache_file_name_base: str
    ):
        super(CacheMaker, self).__init__()
        self.setAutoDelete(False)
        self.parent: GUIMainWindow = parent
        self.pickle_file_path = pickle_file_path
        self.cache_file_name_base: str = cache_file_name_base
        # self.emitter = MyEmitter()
        print("Thread {index} created...".format(index=pickle_file_path))

        if not path.exists(cache_file_name_base):
            mkdir(cache_file_name_base)

    def run(self):
        # print(self.index)
        # print(self.pickle_file_path)

        with open(self.pickle_file_path, "rb") as pickle_file:
            img_dict: dict = pickle.loads(zstd.loads(pickle.load(pickle_file)))

        other_keys: list = [
            x
            for x in img_dict.keys()
            if x != "output_img_dict" and x != "image_index" and x != "composite_img"
        ]
        # print(other_keys)
        image_index = img_dict["image_index"]
        output_img_keys: list = img_dict["output_img_dict"].keys()

        for key in output_img_keys:
            img_name = path.join(self.cache_file_name_base, key + ".png")
            # print(img_name)
            # print(img_dict["output_img_dict"][key])
            img_dict["output_img_dict"][key].save(img_name)

        for key in other_keys:
            img_name = path.join(self.cache_file_name_base, key + ".png")
            # print(img_name)
            img_dict[key].save(img_name)


if __name__ == "__main__":
    app = QtWidgets.QApplication()
    qt_app = GUIMainWindow()
    qt_app.show()
    app.exec_()
