# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'window.ui'
##
## Created by: Qt User Interface Compiler version 5.14.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import (QCoreApplication, QDate, QDateTime, QMetaObject,
    QObject, QPoint, QRect, QSize, QTime, QUrl, Qt)
from PySide2.QtGui import (QBrush, QColor, QConicalGradient, QCursor, QFont,
    QFontDatabase, QIcon, QKeySequence, QLinearGradient, QPalette, QPainter,
    QPixmap, QRadialGradient)
from PySide2.QtWidgets import *


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        if not MainWindow.objectName():
            MainWindow.setObjectName(u"MainWindow")
        MainWindow.resize(1147, 609)
        sizePolicy = QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(MainWindow.sizePolicy().hasHeightForWidth())
        MainWindow.setSizePolicy(sizePolicy)
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName(u"centralwidget")
        sizePolicy1 = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        sizePolicy1.setHorizontalStretch(0)
        sizePolicy1.setVerticalStretch(0)
        sizePolicy1.setHeightForWidth(self.centralwidget.sizePolicy().hasHeightForWidth())
        self.centralwidget.setSizePolicy(sizePolicy1)
        self.verticalLayout = QVBoxLayout(self.centralwidget)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.button_invalidate_cache = QPushButton(self.centralwidget)
        self.button_invalidate_cache.setObjectName(u"button_invalidate_cache")
        self.button_invalidate_cache.setMinimumSize(QSize(120, 0))

        self.horizontalLayout.addWidget(self.button_invalidate_cache)

        self.line_2 = QFrame(self.centralwidget)
        self.line_2.setObjectName(u"line_2")
        self.line_2.setFrameShape(QFrame.VLine)
        self.line_2.setFrameShadow(QFrame.Sunken)

        self.horizontalLayout.addWidget(self.line_2)

        self.text_edit_directory = QPlainTextEdit(self.centralwidget)
        self.text_edit_directory.setObjectName(u"text_edit_directory")
        sizePolicy2 = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Maximum)
        sizePolicy2.setHorizontalStretch(0)
        sizePolicy2.setVerticalStretch(0)
        sizePolicy2.setHeightForWidth(self.text_edit_directory.sizePolicy().hasHeightForWidth())
        self.text_edit_directory.setSizePolicy(sizePolicy2)
        self.text_edit_directory.setMaximumSize(QSize(16777215, 30))

        self.horizontalLayout.addWidget(self.text_edit_directory)

        self.button_select_directory = QPushButton(self.centralwidget)
        self.button_select_directory.setObjectName(u"button_select_directory")
        self.button_select_directory.setMinimumSize(QSize(120, 0))
        self.button_select_directory.setMaximumSize(QSize(16777215, 16777215))

        self.horizontalLayout.addWidget(self.button_select_directory)

        self.button_load = QPushButton(self.centralwidget)
        self.button_load.setObjectName(u"button_load")
        self.button_load.setMinimumSize(QSize(120, 0))

        self.horizontalLayout.addWidget(self.button_load)


        self.verticalLayout.addLayout(self.horizontalLayout)

        self.line = QFrame(self.centralwidget)
        self.line.setObjectName(u"line")
        self.line.setFrameShape(QFrame.HLine)
        self.line.setFrameShadow(QFrame.Sunken)

        self.verticalLayout.addWidget(self.line)

        self.horizontalLayout_2 = QHBoxLayout()
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.image_title_list_widget = QListWidget(self.centralwidget)
        self.image_title_list_widget.setObjectName(u"image_title_list_widget")
        sizePolicy1.setHeightForWidth(self.image_title_list_widget.sizePolicy().hasHeightForWidth())
        self.image_title_list_widget.setSizePolicy(sizePolicy1)
        self.image_title_list_widget.setMinimumSize(QSize(150, 0))
        self.image_title_list_widget.setMaximumSize(QSize(150, 16777215))

        self.horizontalLayout_2.addWidget(self.image_title_list_widget)

        self.tab_image_widget = QTabWidget(self.centralwidget)
        self.tab_image_widget.setObjectName(u"tab_image_widget")

        self.horizontalLayout_2.addWidget(self.tab_image_widget)


        self.verticalLayout.addLayout(self.horizontalLayout_2)

        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QMenuBar(MainWindow)
        self.menubar.setObjectName(u"menubar")
        self.menubar.setGeometry(QRect(0, 0, 1147, 21))
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QStatusBar(MainWindow)
        self.statusbar.setObjectName(u"statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)

        self.tab_image_widget.setCurrentIndex(-1)


        QMetaObject.connectSlotsByName(MainWindow)
    # setupUi

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QCoreApplication.translate("MainWindow", u"Image Viewer", None))
        self.button_invalidate_cache.setText(QCoreApplication.translate("MainWindow", u"Invalidate Cache", None))
        self.text_edit_directory.setPlainText(QCoreApplication.translate("MainWindow", u"Z:GUI_Test_Images", None))
        self.button_select_directory.setText(QCoreApplication.translate("MainWindow", u"Search and Load", None))
        self.button_load.setText(QCoreApplication.translate("MainWindow", u"Load Current", None))
    # retranslateUi

