# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'ImageForm.ui'
##
## Created by: Qt User Interface Compiler version 5.14.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import (QCoreApplication, QDate, QDateTime, QMetaObject,
    QObject, QPoint, QRect, QSize, QTime, QUrl, Qt)
from PySide2.QtGui import (QBrush, QColor, QConicalGradient, QCursor, QFont,
    QFontDatabase, QIcon, QKeySequence, QLinearGradient, QPalette, QPainter,
    QPixmap, QRadialGradient)
from PySide2.QtWidgets import *


class Ui_ImageForm(object):
    def setupUi(self, ImageForm):
        if not ImageForm.objectName():
            ImageForm.setObjectName(u"ImageForm")
        ImageForm.resize(885, 515)
        sizePolicy = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(ImageForm.sizePolicy().hasHeightForWidth())
        ImageForm.setSizePolicy(sizePolicy)
        self.verticalLayout = QVBoxLayout(ImageForm)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.horizontalLayout_inputs = QHBoxLayout()
        self.horizontalLayout_inputs.setObjectName(u"horizontalLayout_inputs")
        self.verticalLayout_2 = QVBoxLayout()
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.label_3 = QLabel(ImageForm)
        self.label_3.setObjectName(u"label_3")

        self.verticalLayout_2.addWidget(self.label_3)

        self.line_5 = QFrame(ImageForm)
        self.line_5.setObjectName(u"line_5")
        self.line_5.setFrameShape(QFrame.HLine)
        self.line_5.setFrameShadow(QFrame.Sunken)

        self.verticalLayout_2.addWidget(self.line_5)

        self.graphics_original = QLabel(ImageForm)
        self.graphics_original.setObjectName(u"graphics_original")
        sizePolicy.setHeightForWidth(self.graphics_original.sizePolicy().hasHeightForWidth())
        self.graphics_original.setSizePolicy(sizePolicy)
        self.graphics_original.setScaledContents(True)
        self.graphics_original.setAlignment(Qt.AlignCenter)

        self.verticalLayout_2.addWidget(self.graphics_original)


        self.horizontalLayout_inputs.addLayout(self.verticalLayout_2)

        self.horizontalSpacer = QSpacerItem(5, 20, QSizePolicy.Fixed, QSizePolicy.Minimum)

        self.horizontalLayout_inputs.addItem(self.horizontalSpacer)

        self.verticalLayout_3 = QVBoxLayout()
        self.verticalLayout_3.setObjectName(u"verticalLayout_3")
        self.label_4 = QLabel(ImageForm)
        self.label_4.setObjectName(u"label_4")

        self.verticalLayout_3.addWidget(self.label_4)

        self.line_6 = QFrame(ImageForm)
        self.line_6.setObjectName(u"line_6")
        self.line_6.setFrameShape(QFrame.HLine)
        self.line_6.setFrameShadow(QFrame.Sunken)

        self.verticalLayout_3.addWidget(self.line_6)

        self.graphics_mask = QLabel(ImageForm)
        self.graphics_mask.setObjectName(u"graphics_mask")
        sizePolicy.setHeightForWidth(self.graphics_mask.sizePolicy().hasHeightForWidth())
        self.graphics_mask.setSizePolicy(sizePolicy)
        self.graphics_mask.setScaledContents(True)
        self.graphics_mask.setAlignment(Qt.AlignCenter)

        self.verticalLayout_3.addWidget(self.graphics_mask)


        self.horizontalLayout_inputs.addLayout(self.verticalLayout_3)


        self.verticalLayout.addLayout(self.horizontalLayout_inputs)

        self.horizontalLayout_generated = QHBoxLayout()
        self.horizontalLayout_generated.setObjectName(u"horizontalLayout_generated")
        self.verticalLayout_outputs = QVBoxLayout()
        self.verticalLayout_outputs.setObjectName(u"verticalLayout_outputs")
        self.label = QLabel(ImageForm)
        self.label.setObjectName(u"label")

        self.verticalLayout_outputs.addWidget(self.label)

        self.line_3 = QFrame(ImageForm)
        self.line_3.setObjectName(u"line_3")
        self.line_3.setFrameShape(QFrame.HLine)
        self.line_3.setFrameShadow(QFrame.Sunken)

        self.verticalLayout_outputs.addWidget(self.line_3)

        self.tab_output_img_widget = QTabWidget(ImageForm)
        self.tab_output_img_widget.setObjectName(u"tab_output_img_widget")

        self.verticalLayout_outputs.addWidget(self.tab_output_img_widget)


        self.horizontalLayout_generated.addLayout(self.verticalLayout_outputs)

        self.horizontalSpacer_2 = QSpacerItem(5, 20, QSizePolicy.Fixed, QSizePolicy.Minimum)

        self.horizontalLayout_generated.addItem(self.horizontalSpacer_2)

        self.verticalLayout_feature_extractions = QVBoxLayout()
        self.verticalLayout_feature_extractions.setObjectName(u"verticalLayout_feature_extractions")
        self.label_2 = QLabel(ImageForm)
        self.label_2.setObjectName(u"label_2")

        self.verticalLayout_feature_extractions.addWidget(self.label_2)

        self.line_4 = QFrame(ImageForm)
        self.line_4.setObjectName(u"line_4")
        self.line_4.setFrameShape(QFrame.HLine)
        self.line_4.setFrameShadow(QFrame.Sunken)

        self.verticalLayout_feature_extractions.addWidget(self.line_4)

        self.tab_feature_img_widget = QTabWidget(ImageForm)
        self.tab_feature_img_widget.setObjectName(u"tab_feature_img_widget")

        self.verticalLayout_feature_extractions.addWidget(self.tab_feature_img_widget)


        self.horizontalLayout_generated.addLayout(self.verticalLayout_feature_extractions)


        self.verticalLayout.addLayout(self.horizontalLayout_generated)


        self.retranslateUi(ImageForm)

        self.tab_output_img_widget.setCurrentIndex(-1)


        QMetaObject.connectSlotsByName(ImageForm)
    # setupUi

    def retranslateUi(self, ImageForm):
        ImageForm.setWindowTitle(QCoreApplication.translate("ImageForm", u"Form", None))
        self.label_3.setText(QCoreApplication.translate("ImageForm", u"Original Image:", None))
        self.graphics_original.setText("")
        self.label_4.setText(QCoreApplication.translate("ImageForm", u"Segmentation Image:", None))
        self.graphics_mask.setText("")
        self.label.setText(QCoreApplication.translate("ImageForm", u"Output images:", None))
        self.label_2.setText(QCoreApplication.translate("ImageForm", u"Feature extraction images:", None))
    # retranslateUi

